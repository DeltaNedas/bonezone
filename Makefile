BUILDDIR ?= build
OBJECTDIR ?= objects
SOURCEDIR ?= src
INCLUDEDIR ?= include

CC := gcc
STANDARD := c99
CFLAGS ?= -O3 -Wall -pedantic -g
CFLAGS += -std=$(STANDARD) -c -I$(INCLUDEDIR)
LDFLAGS ?= '-Wl,-rpath,$$ORIGIN'

BINARY := bonezone
SOURCES := $(shell find $(SOURCEDIR)/ -type f -name "*.c")
OBJECTS := $(patsubst $(SOURCEDIR)/%, $(OBJECTDIR)/%.o, $(SOURCES))
DEPENDS := $(patsubst $(SOURCEDIR)/%, $(OBJECTDIR)/%.d, $(SOURCES))

all: $(BUILDDIR)/$(BINARY)

$(OBJECTDIR)/%.o: $(SOURCEDIR)/%
	@printf "CC\t$@\n"
	@mkdir -p `dirname $@`
	@$(CC) $(CFLAGS) -MMD -MP $< -o $@

-include $(DEPENDS)

$(BUILDDIR)/$(BINARY): $(OBJECTS)
	@printf "CCLD\t$@\n"
	@mkdir -p $(BUILDDIR)
	@$(CC) $^ -o $@ $(LDFLAGS)

clean:
	rm -rf $(OBJECTDIR)

run: all
	@$(BUILDDIR)/$(BINARY)
