#pragma once

#include "types.h"

/* Draw a temporary status under the game */
void draw_status(char *fmt, ...);
/* Draw to the right of the game at Y=level */
void draw_info(uint level, char *fmt, ...);

void clear_status();
void clear_info(uint level);

/* Character to print for a certain tile */
char char_drawn(uchar tile);
/* Draw placable tiles and the selector */
void draw_editor();
/* Draw effects and tile player is above */
void draw_infos();
/* Draw the game's Tiles */
void draw_tiles();

void cleanup(uint y);

/* Tile selected in the editor */
extern int selection;
