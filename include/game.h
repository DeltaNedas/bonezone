#pragma once

#include "types.h"

typedef struct {
	uint width, height;
	uint spawn_x, spawn_y;
	uchar *tiles;
	/* Current level, 0 for custom map */
	int level;
	/* Path to save to with [O], NULL to disable. */
	char *path;
} game_t;

uchar get_tile(uint x, uint y);
void set_tile(uint x, uint y, uchar tile);

void new_game(int argc, char **argv);
/* Returns for normal errors, exits for EIO/ENOMEM */
int load_game(char *path);
void save_game(char *path);
void run_game();
void free_game();

int next_level(int starting, int level);
/* Quit or move on to next level */
void game_won();

extern game_t game;
