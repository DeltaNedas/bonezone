#pragma once

#include "types.h"

typedef enum {
	STICKY = 0x01,
	INSULATED = 0x02,
	KEY = 0x04,
	/* Set to disable soap sliding */
	SPAWNED = 0x08,
	/* Show letters for all tiles */
	DETAILED = 0x10,
	EDITING = 0x20,

	EFFECTS = STICKY | INSULATED,
	LOSE_ON_DEATH = EFFECTS | KEY
} flags_t;

typedef struct {
	flags_t flags;
	char under;
	uint x, y;
} player_t;

void move_player(uint x, uint y);
void kill_player();
void spawn_player();

extern player_t player;
