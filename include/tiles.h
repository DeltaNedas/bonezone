#pragma once

#include "types.h"

enum {
	T_ERROR,
	T_PLAYER,

	T_NONE,
	T_WALL,
	T_EXIT,
	T_WATER,
	T_ELECTRICITY,
	T_AUTOPHOBIC,
	T_GATE,

	T_SYRUP,
	T_INSULATION,
	T_KEY,
	T_SOAP,
	T_RESET
};

char *tile_colour(uchar tile);
char *kill_status(uchar tile);

/* Returns:
	0 - Normal tile
	1 - Don't set position
	2 - Kill
	3 - Kill but don't redraw
	4 - Redraw infos
*/
int player_moved(uint x, uint y, uchar tile);
int can_move(uint x, uint y);

typedef struct {
	char symbol;
	char *name, *description, *colour;
} tile_t;

extern tile_t tiles[];
extern int tile_count;
/* Array of T_... */
extern uchar placable_tiles[];
extern int placable_count;
