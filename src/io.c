#include "game.h"

#include "player.h"
#include "tiles.h"
#include "util.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
	char w, h;
	char s_x, s_y;
} header_t;

int valid() {
	return game.width > 0 && game.height > 0
		&& game.spawn_x < game.width
		&& game.spawn_y < game.height
		&& game.spawn_x >= 0 && game.spawn_y >= 0;
}

int load_game(char *path) {
	FILE *f = fopen(path, "r");
	if (!f) {
		return -1;
	}

	header_t buff;
	if (fread(&buff, 1, 4, f) != 4) {
		int eof = feof(f);
		int err = ferror(f);
		fclose(f);

		if (eof) {
			fprintf(stderr, "Save file at %s is too small\n", path);
			return 1;
		}
		fprintf(stderr, "Failed to read save file at %s: %s (%d)", path, strerror(err), err);
		quit(err);
	}

	game.width = buff.w;
	game.height = buff.h;
	game.spawn_x = buff.s_x;
	game.spawn_y = buff.s_y;
	if (!valid()) {
		fprintf(stderr, "Invalid map\n");
		return 2;
	}

	int area = game.width * game.height;
	if (game.tiles) free(game.tiles);
	if (!(game.tiles = malloc(area))) {
		fprintf(stderr, "Failed to allocate %d bytes of memory for tiles: %s (%d)\n", area, strerror(errno), errno);
		quit(errno);
	}

	if (fread(game.tiles, 1, area, f) != area) {
		int eof = feof(f);
		int err = ferror(f);
		fclose(f);

		if (eof) {
			fprintf(stderr, "Save file at %s is too small\n", path);
			return 3;
		}
		fprintf(stderr, "Failed to read save data at %s: %s (%d)\n", path, strerror(err), err);
		quit(err);
	}
	fclose(f);

	/* Make sure no tiles are out of bounds */
	for (int i = 0; i < area; i++) {
		if (game.tiles[i] >= tile_count) {
			fprintf(stderr, "Tile at %d, %d is out of bounds!\n", i % game.width, i / game.height);
			quit(-1);
		}
	}
	return 0;
}

void save_game(char *path) {
	FILE *f = fopen(path, "wb");
	header_t header;
	header.w = (char) game.width;
	header.h = (char) game.height;
	header.s_x = (char) game.spawn_x;
	header.s_y = (char) game.spawn_y;

	int area = game.width * game.height;
	set_tile(player.x, player.y, player.under);
	if ((fwrite(&header, 1, sizeof(header_t), f) != sizeof(header_t))
		|| (fwrite(game.tiles, 1, area, f) != area)) {
		quit(1);
	}
	fclose(f);
	set_tile(player.x, player.y, T_PLAYER);
}
