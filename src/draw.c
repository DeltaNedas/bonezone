#include "draw.h"

#include "game.h"
#include "player.h"
#include "tiles.h"

#include <stdarg.h>
#include <stdio.h>

#define DRAW_TILE(tile) \
	printf("\033[%sm%c", tile_colour(tile), char_drawn(tile));
#define FLINFO(flag, ...) \
	if (player.flags & flag) { \
		draw_info(flags++, __VA_ARGS__); \
	}

void draw_status(char *fmt, ...) {
	clear_status();
	va_list args;
	va_start(args, fmt);
	vprintf(fmt, args);
	va_end(args);
	fflush(stdout);
}

void draw_info(uint level, char *fmt, ...) {
	clear_info(level);
	va_list args;
	va_start(args, fmt);
	vprintf(fmt, args);
	va_end(args);
	fflush(stdout);
}

void clear_status() {
	printf("\033[%d;2H\033[K", game.height + 2);
}

void clear_info(uint level) {
	printf("\033[%d;%dH\033[K", level, game.width + 2);
}

char char_drawn(uchar tile) {
	if (tile == T_PLAYER) return tiles[tile].symbol;

	return player.flags & DETAILED ? tiles[tile].symbol : ' ';
}

void draw_editor() {
	printf("\033[%d;2H", game.height + 4);
	tile_t tile;
	for (uint i = 0; i < placable_count; i++) {
		tile = tiles[placable_tiles[i]];
		printf("\033[%sm%c", tile.colour, tile.symbol);
	}

	/* Clear old selector, draw new */
	printf("\033[0m\n\033[%dG\033[2K^", selection + 2);
	fflush(stdout);
}

void draw_infos() {
	tile_t tile = tiles[(int) player.under];
	draw_info(1, "Above [\033[%sm%c\033[0m]", tile.colour, tile.symbol);
	int flags = 3;
	FLINFO(STICKY, "\033[%smSticky\033[0m", tile_colour(T_SYRUP));
	FLINFO(INSULATED, "\033[%smInsulated\033[0m", tile_colour(T_INSULATION));
	FLINFO(KEY, "\033[%smKey\033[0m", tile_colour(T_KEY));
	for (int i = flags; i < 6; i++) {
		clear_info(i);
	}
}

void draw_tiles() {
	printf("\033[1;1H");
	for (uint y = 0; y < game.height; y++) {
		uchar *row = game.tiles + y * game.width;
		for (uint x = 0; x < game.width; x++) {
			DRAW_TILE(row[x]);
		}
		printf("\033[0m\n");
	}
}

void cleanup(uint h) {
	printf("\033[1;1H");
	/* h + status + editor */
	for (uint y = 0; y < h + 4; y++) {
		printf("\033[K\n");
	}
}

/* Index of tile selected in the editor */
int selection = 0;
