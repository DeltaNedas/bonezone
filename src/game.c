#include "game.h"

#include "draw.h"
#include "player.h"
#include "text.h"
#include "tiles.h"
#include "util.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#define POS(x, y) ((unsigned long) x << 32) | ((long) y & 0xFFFFFFFF)
#define OPT_EQ(sh, lo) !strcmp(arg, "-" sh) || !strcmp(arg, "--" lo)
#define OPT_CHK(n) \
	if (argc <= i + n) { \
		fprintf(stderr, "Expected %d arguments for option #%d '%s'\n", n, i, arg); \
		exit(EINVAL); \
	}
#define OPT_INVAL(n, type) \
	fprintf(stderr, "Invalid argument #%d (Expected a %s)\n", i + n, type); \
	exit(EINVAL);

#define D_UP POS(0, -1)
#define D_LEFT POS(-1, 0)
#define D_DOWN POS(0, 1)
#define D_RIGHT POS(1, 0)

uchar get_tile(uint x, uint y) {
	if (x < 0 || y < 0 || x > game.width || y > game.height) {
		return T_ERROR;
	}
	return game.tiles[x + y * game.width];
}

void set_tile(uint x, uint y, uchar tile) {
	game.tiles[x + y * game.width] = tile;
}

void create_map() {
	printf("Enter new map size (WxH): ");
	if (scanf("%ux%u", &game.width, &game.height) != 2) {
		fprintf(stderr, "Invalid size!\n");
		exit(1);
	}

	uint area = game.width * game.height;
	if (!(game.tiles = malloc(area))) {
		perror("Failed to allocate tiles for map:");
		exit(errno);
	}

	memset(game.tiles, T_NONE, area);
	game.spawn_x = game.spawn_y = 0;
}

void new_game(int argc, char **argv) {
	player.flags = 0;
	game.tiles = NULL;
	game.path = NULL;
	game.level = 1;
	char *arg, *map = NULL;
	for (int i = 1; i < argc; i++) {
		arg = argv[i];
		if (OPT_EQ("l", "level")) {
			OPT_CHK(1);
			errno = 0;
			game.level = atoi(argv[++i]);
			if (errno) {
				OPT_INVAL(1, "number");
			}
		} else if (OPT_EQ("m", "map")) {
			OPT_CHK(1);
			map = argv[++i];
			game.level = 0;
		} else if (OPT_EQ("d", "detailed")) {
			player.flags |= DETAILED;
		} else if (OPT_EQ("e", "editor")) {
			OPT_CHK(1);
			game.path = map = argv[++i];
			player.flags |= EDITING;
		/* Documentation */
		} else if (OPT_EQ("t", "tiles")) {
			print_tiles(); exit(0);
		} else if (OPT_EQ("h", "help")) {
			print_help(); exit(0);
		} else if (OPT_EQ("M", "minecraft")) {
			print_link(); exit(0);
		} else if (OPT_EQ("C", "copyright")) {
			print_copyright(); exit(0);
		} else {
			fprintf(stderr, "Unknown argument #%d: '%s' (Use --help)\n", i, arg);
			exit(EINVAL);
		}
	}

	int my_path = !map;
	if (my_path) {
		map = malloc(32);
		sprintf(map, "levels/%d.bzm", game.level);
	}

	int code = load_game(map);
	if (my_path) free(map);
	if (code) {
		if (game.path) {
			create_map();
		} else {
			exit(1);
		}
	}
	spawn_player();
}

void run_game() {
	cleanup(game.height);
	draw_tiles();
	draw_infos();
	if (game.path) {
		draw_editor();
	}

	char c;
	while (1) {
		c = getch();
		long dir = 0;
		switch (c) {
		/* ^D to exit */
		case 4:
			draw_status("You left the Bone Zone.\n");
			/* Put shell on line after editor */
			if (game.path) {
				printf("\n\n");
			}
			quit(0);
		case 'w': dir = D_UP; break;
		case 'a': dir = D_LEFT; break;
		case 's': dir = D_DOWN; break;
		case 'd': dir = D_RIGHT; break;
		case 'o':
			if (game.path) {
				save_game(game.path);
				draw_status("Saved to %s", game.path);
			}
			break;
		case 't':
			if (game.path) {
				if (player.flags & EDITING) {
					kill_player();
					draw_tiles();
					player.flags &= ~EDITING;
					draw_status("You are now testing.");
				} else {
					player.flags |= EDITING;
					draw_status("You are now editing.");
				}
			}
			break;
		}

		if (player.flags & EDITING) {
			switch (c) {
			case 'p':
				game.spawn_x = player.x;
				game.spawn_y = player.y;
				draw_status("Spawn set to %d, %d", player.x, player.y);
				break;

			/* Tile placing */
			case 'q':
				/* % doesn't wrap -1 to max - 1 */
				selection = (placable_count + selection - 1) % placable_count;
				draw_editor();
				break;
			case 'e':
				selection = (selection + 1) % placable_count;
				draw_editor();
				break;
			case ' ': {
				uchar tile = placable_tiles[selection];
				player.under = tile;
				draw_tiles();
				draw_infos();
				tile_t tilet = tiles[tile];
				draw_status("Placed \033[%sm%c\033[0m at %d, %d", tilet.colour, tilet.symbol, player.x + 1, player.y + 1);
				break;
			}
			}
		}

		if (dir) {
			int x = player.x + (dir >> 32), y = player.y + (int) dir;
			if (can_move(x, y)) {
				clear_status();
				move_player(x, y);
				draw_tiles();
				draw_infos();
			} else {
				draw_status("You can't move there!");
			}
		}
	}
}

void free_game() {
	if (game.tiles) free(game.tiles);
}

int next_level(int starting, int level) {
	char *path = malloc(32);
	sprintf(path, "levels/%d.bzm", level);
	uint h = game.height;
	int code = load_game(path);
	free(path);
	if (!code) {
		spawn_player();
		if (starting) {
			cleanup(h);
			draw_tiles();
		}
		draw_status("Welcome to level %d of the \033[1;97mBone Zone\033[0m", level);
		return 1;
	}
	return 0;
}

void game_won() {
	if (game.path) {
		player.flags |= EDITING;
		draw_status("Good job! Press \033[1mO\033[0m to save it.");
		/* Prevent duplicating the exit */
		player.under = get_tile(player.x, player.y);
		return;
	}

	errno = 0;
	if (game.level) {
		if (next_level(1, ++game.level)) {
			return;
		}
	}

	draw_tiles();
	draw_infos();
	draw_status("\033[38;5;40mYou escaped the \033[1;97mBone Zone\033[0m\n");
	quit(0);
}

game_t game;
