#include "tiles.h"

#include "draw.h"
#include "game.h"
#include "player.h"
#include "util.h"

#include <stdio.h>

#define ZAP(x, y) get_tile(x, y) == T_ELECTRICITY

char *tile_colour(uchar tile) {
	if (tile == T_PLAYER) tile = player.under;
	return tiles[tile].colour;
}

char *kill_status(uchar tile) {
	switch (tile) {
	case T_WATER:
		return "\033[31mPiranhas ate your sticky bones!\033[0m";
	case T_ELECTRICITY:
		return "\033[31mYou were \033[93mElectrocuted!\033[0m";
	case T_GATE:
	case T_WALL:
		return "\033[31mYou were smashed into a wall!\033[0m";
	}
	return "\033[31mYou died!\033[0m";
}

int player_moved(uint x, uint y, uchar tile) {
	if (player.flags & EDITING) {
		return 0;
	}

	int dx = x - player.x;
	int dy = y - player.y;
	char spawned = player.flags & SPAWNED;

	switch (tile) {
	case T_EXIT:
		game_won();
		return 1;
	case T_SYRUP:
		player.flags |= STICKY;
		return 4;
	case T_KEY:
		player.flags |= KEY;
		return 4;
	case T_SOAP:
		player.flags &= ~STICKY;
		draw_infos();
		if ((dx || dy) && !spawned) {
			player.x = x;
			player.y = y;
			move_player(x + dx, y + dy);
			draw_tiles();
			return 1;
		}
		break;
	case T_WATER:
		if (player.flags & STICKY) {
			return 2;
		}
		if (!(player.flags & INSULATED)
				&& (ZAP(x, y + 1)
				|| ZAP(x, y - 1)
				|| ZAP(x + 1, y)
				|| ZAP(x - 1, y))) {
			draw_tiles();
			draw_status("%s", kill_status(T_ELECTRICITY));
			return 3;
		}
		break;
	case T_WALL:
	case T_ELECTRICITY:
		/* Insulation is too weak for electricity itself */
		return 2;
	case T_INSULATION:
		player.flags |= INSULATED;
		return 4;
	case T_RESET:
		player.flags &= ~EFFECTS;
		return 4;
	/* Soap doesnt care if gate is locked */
	case T_GATE:
		if (!(player.flags & KEY)) {
			return 2;
		}
	}
	return 0;
}

int can_move(uint x, uint y) {
	if (x >= game.width || y >= game.height) {
		return 0;
	}

	if (player.flags & EDITING) {
		return 1;
	}

	uchar tile = get_tile(x, y);
	switch (tile) {
	case T_WALL:
	case T_PLAYER: /* what the fuck */
	case T_ERROR:
		return 0;
	case T_AUTOPHOBIC:
		return player.under != T_AUTOPHOBIC;
	case T_GATE:
		return player.flags & KEY;
	}
	return 1;
}

tile_t tiles[] = {
	/* Special tiles */
	{'?', "Error", "You shouldn't ever see this.", "41;30"},
	/* p#Volour comes from player.under */
	{'P', "Player", "You.\n", "0"},

	/* Normal tiles */
	{' ', "Empty Tile", "Always walkable.", "107;30"},
	{'#', "Wall", "Impassable.", "41;30"},
	{'X', "Exit", "Get here to win the level.", "40;97"},
	{'~', "Water", "Home to syrup-loving Piranhas.\n\tDecent conductor.", "44;97"},
	{'*', "Electricity", "Electrifies adjacent Water tiles, lethal.", "48;5;226;30"},
	{'A', "Autophobic Tile", "Impassable from adjacent Autophobic Tiles.", "42;97"},
	{'G', "Gate", "Impassable unless you have a key.\n", "48;5;94;97"},

	/* Effects */
	/* the goal of entering the bone zone */
	{'S', "Syrup", "Covers your bones in sticky syrup.", "43;97"},
	{'I', "Insulation", "Protects you from electrified Water.\n\tToo weak against Electricity itself.", "48;5;219;30"},
	{'K', "Key", "Unlocks gates when collected.", "100;97"},
	{'+', "Soap", "Cleans you of syrup and slides you to the other side.\n\tWalls and locked gates will crush you.", "45;97"},
	{'@', "Reset", "Removes Syrup and Insulation.", "48;5;81;30"}
};

uchar placable_tiles[] = {
	T_NONE, T_WALL,
	T_EXIT, T_WATER,
	T_ELECTRICITY, T_AUTOPHOBIC,
	T_GATE,

	T_SYRUP, T_INSULATION, T_KEY,
	T_SOAP, T_RESET
};

int tile_count = sizeof(tiles) / sizeof(tile_t);
int placable_count = sizeof(placable_tiles);
