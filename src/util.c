#include "util.h"

#include "game.h"

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>

void quit(int code) {
	free_game();
	exit(code);
}

/* Get a single character and don't echo */
char getch() {
	struct termios old, new;
	char c;
	if (tcgetattr(STDIN_FILENO, &old)) {
		perror("Failed to get terminal settings:");
		quit(69);
	}
	new = old;

	new.c_iflag = 0;
	new.c_oflag &= ~OPOST;
	new.c_lflag &= ~(ISIG | ICANON | ECHO);
	new.c_cc[VMIN] = 1;
	new.c_cc[VTIME] = 0;
	if (tcsetattr(STDIN_FILENO, TCSANOW, &new)) {
		perror("Failed to set terminal settings:");
		quit(70);
	}

	if (read(STDIN_FILENO, &c, 1) < 0) {
		perror("Failed to read input:");
		quit(71);
	}

	if (tcsetattr(STDIN_FILENO, TCSADRAIN, &old)) {
		perror("Failed to reset terminal settings:");
		quit(72);
	}

	return c;
}
