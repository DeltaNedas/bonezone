#include "text.h"

#include "tiles.h"

#include <stdio.h>

void print_help() {
	printf(
	"BoneZone is a tile game inspired by the Minecraft map by Henzoid.\n" \
	"Contains absolutely references to Undertale.\n" \
	"Use ^D to quit, WASD to move.\n" \
	"Usage: bonezone [options]\n" \
	"Available options:\n" \
	"\t-l/--level <num> - Select a level.\n" \
	"\t\t(Default is 1)\n" \
	"\t-m/--map <path> - Load a map.\n" \
	"\t\tOverrides built-in levels.\n" \
	"\t-d/--detailed - Show every tile's letter.\n" \
	"\t-e/--editor <path> - Set path to save map\n" \
	"\t\tPress O to save map to path.\n" \
	"\t\tPress Q/E to select a tile to place.\n" \
	"\t\tPress Space to place selected tile under the player.\n" \
	"\t\tPress T to test it, again to exit test mode.\n" \
	"\t\tPress P to set spawn point to your position.\n" \
	"\t-t/--tiles - Show help on tiles.\n" \
	"\t-h/--help - Show this information.\n" \
	"\t-M/--minecraft - Link the Minecraft map.\n" \
	"\t-C/--copyright - Show copyright information.\n\n" \
	"You can get the source code from https://bitbucket.org/DeltaNedas/bonezone\n");
}

void print_copyright() {
	printf("BoneZone Copyright (C) 2020 DeltaNedas\n" \
	"This program is free software: you can redistribute it and/or modify\n" \
	"it under the terms of the GNU General Public License as published by\n" \
	"the Free Software Foundation, either version 3 of the License, or\n" \
	"(at your option) any later version.\n\n" \
	"This program is distributed in the hope that it will be useful,\n" \
	"but WITHOUT ANY WARRANTY; without even the implied warranty of\n" \
	"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the\n" \
	"GNU General Public License for more details.\n\n" \
	"You should have received a copy of the GNU General Public License\n" \
	"along with this program. If not, see <https://www.gnu.org/licenses/>.\n");
}

void print_link() {
	printf("Get the minecraft map here:\n" \
		"\thttps://www.planetminecraft.com/project/the-bone-zone/\n");
}

void print_tiles() {
	printf("All tiles in the \033[97mBoneZone\033[0m:\n");
	tile_t tile;
	for (int i = 0; i < tile_count; i++) {
		tile = tiles[i];
		printf("\033[%sm%c\033[0m - %s - %s\n", tile.colour, tile.symbol, tile.name, tile.description);
	}
	printf("---\nSome tiles grant effects that are removed when you die.\n");
}
