#include "player.h"

#include "draw.h"
#include "game.h"
#include "tiles.h"

#include <stdio.h>

void move_player(uint x, uint y) {
	set_tile(player.x, player.y, player.under);
	uchar tile = player.under = get_tile(x, y);
	/* Run the effects of a certain tile */
	int code = player_moved(x, y, tile);
	if (code == 1) return;

	set_tile(x, y, T_PLAYER);
	if (code == 0) {
		draw_tiles();
	}

	player.x = x;
	player.y = y;
	if (code == 2 || code == 3) {
		kill_player();
		if (code == 2) {
			draw_tiles();
			draw_status("%s", kill_status(tile));
		}
	}

	if (code == 4) {
		draw_tiles();
		draw_infos();
	}
}

void kill_player() {
	player.flags &= ~LOSE_ON_DEATH;
	player.flags |= SPAWNED;
	move_player(game.spawn_x, game.spawn_y);
	player.flags &= ~SPAWNED;
	draw_infos();
}

void spawn_player() {
	uint x = game.spawn_x, y = game.spawn_y;
	player.x = x;
	player.y = y;
	player.under = get_tile(x, y);
	set_tile(x, y, T_PLAYER);
	player.flags |= SPAWNED;
	player_moved(x, y, player.under);
	player.flags &= ~SPAWNED;
	draw_infos();
}

player_t player;
